const path = require('path');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/init.mjs',
  output: {
    publicPath: '/',
    filename: 'word-code-core.js',
    path: path.resolve(__dirname, 'dist'),
    library: {
      // 外部引入名称: import Graphic from 'word-code'
      name: "word-code",
      type: 'umd',
    },
  },
  plugins: [
    new NodemonPlugin(), // Dong
  ],
};
