/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["word-code"] = factory();
	else
		root["word-code"] = factory();
})(self, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/charenc/charenc.js":
/*!*****************************************!*\
  !*** ./node_modules/charenc/charenc.js ***!
  \*****************************************/
/***/ ((module) => {

eval("var charenc = {\n  // UTF-8 encoding\n  utf8: {\n    // Convert a string to a byte array\n    stringToBytes: function(str) {\n      return charenc.bin.stringToBytes(unescape(encodeURIComponent(str)));\n    },\n\n    // Convert a byte array to a string\n    bytesToString: function(bytes) {\n      return decodeURIComponent(escape(charenc.bin.bytesToString(bytes)));\n    }\n  },\n\n  // Binary encoding\n  bin: {\n    // Convert a string to a byte array\n    stringToBytes: function(str) {\n      for (var bytes = [], i = 0; i < str.length; i++)\n        bytes.push(str.charCodeAt(i) & 0xFF);\n      return bytes;\n    },\n\n    // Convert a byte array to a string\n    bytesToString: function(bytes) {\n      for (var str = [], i = 0; i < bytes.length; i++)\n        str.push(String.fromCharCode(bytes[i]));\n      return str.join('');\n    }\n  }\n};\n\nmodule.exports = charenc;\n\n\n//# sourceURL=webpack://word-code/./node_modules/charenc/charenc.js?");

/***/ }),

/***/ "./node_modules/crypt/crypt.js":
/*!*************************************!*\
  !*** ./node_modules/crypt/crypt.js ***!
  \*************************************/
/***/ ((module) => {

eval("(function() {\n  var base64map\n      = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',\n\n  crypt = {\n    // Bit-wise rotation left\n    rotl: function(n, b) {\n      return (n << b) | (n >>> (32 - b));\n    },\n\n    // Bit-wise rotation right\n    rotr: function(n, b) {\n      return (n << (32 - b)) | (n >>> b);\n    },\n\n    // Swap big-endian to little-endian and vice versa\n    endian: function(n) {\n      // If number given, swap endian\n      if (n.constructor == Number) {\n        return crypt.rotl(n, 8) & 0x00FF00FF | crypt.rotl(n, 24) & 0xFF00FF00;\n      }\n\n      // Else, assume array and swap all items\n      for (var i = 0; i < n.length; i++)\n        n[i] = crypt.endian(n[i]);\n      return n;\n    },\n\n    // Generate an array of any length of random bytes\n    randomBytes: function(n) {\n      for (var bytes = []; n > 0; n--)\n        bytes.push(Math.floor(Math.random() * 256));\n      return bytes;\n    },\n\n    // Convert a byte array to big-endian 32-bit words\n    bytesToWords: function(bytes) {\n      for (var words = [], i = 0, b = 0; i < bytes.length; i++, b += 8)\n        words[b >>> 5] |= bytes[i] << (24 - b % 32);\n      return words;\n    },\n\n    // Convert big-endian 32-bit words to a byte array\n    wordsToBytes: function(words) {\n      for (var bytes = [], b = 0; b < words.length * 32; b += 8)\n        bytes.push((words[b >>> 5] >>> (24 - b % 32)) & 0xFF);\n      return bytes;\n    },\n\n    // Convert a byte array to a hex string\n    bytesToHex: function(bytes) {\n      for (var hex = [], i = 0; i < bytes.length; i++) {\n        hex.push((bytes[i] >>> 4).toString(16));\n        hex.push((bytes[i] & 0xF).toString(16));\n      }\n      return hex.join('');\n    },\n\n    // Convert a hex string to a byte array\n    hexToBytes: function(hex) {\n      for (var bytes = [], c = 0; c < hex.length; c += 2)\n        bytes.push(parseInt(hex.substr(c, 2), 16));\n      return bytes;\n    },\n\n    // Convert a byte array to a base-64 string\n    bytesToBase64: function(bytes) {\n      for (var base64 = [], i = 0; i < bytes.length; i += 3) {\n        var triplet = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];\n        for (var j = 0; j < 4; j++)\n          if (i * 8 + j * 6 <= bytes.length * 8)\n            base64.push(base64map.charAt((triplet >>> 6 * (3 - j)) & 0x3F));\n          else\n            base64.push('=');\n      }\n      return base64.join('');\n    },\n\n    // Convert a base-64 string to a byte array\n    base64ToBytes: function(base64) {\n      // Remove non-base-64 characters\n      base64 = base64.replace(/[^A-Z0-9+\\/]/ig, '');\n\n      for (var bytes = [], i = 0, imod4 = 0; i < base64.length;\n          imod4 = ++i % 4) {\n        if (imod4 == 0) continue;\n        bytes.push(((base64map.indexOf(base64.charAt(i - 1))\n            & (Math.pow(2, -2 * imod4 + 8) - 1)) << (imod4 * 2))\n            | (base64map.indexOf(base64.charAt(i)) >>> (6 - imod4 * 2)));\n      }\n      return bytes;\n    }\n  };\n\n  module.exports = crypt;\n})();\n\n\n//# sourceURL=webpack://word-code/./node_modules/crypt/crypt.js?");

/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/*!*****************************************!*\
  !*** ./node_modules/is-buffer/index.js ***!
  \*****************************************/
/***/ ((module) => {

eval("/*!\n * Determine if an object is a Buffer\n *\n * @author   Feross Aboukhadijeh <https://feross.org>\n * @license  MIT\n */\n\n// The _isBuffer check is for Safari 5-7 support, because it's missing\n// Object.prototype.constructor. Remove this eventually\nmodule.exports = function (obj) {\n  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)\n}\n\nfunction isBuffer (obj) {\n  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)\n}\n\n// For Node v0.10 support. Remove this eventually.\nfunction isSlowBuffer (obj) {\n  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))\n}\n\n\n//# sourceURL=webpack://word-code/./node_modules/is-buffer/index.js?");

/***/ }),

/***/ "./node_modules/md5/md5.js":
/*!*********************************!*\
  !*** ./node_modules/md5/md5.js ***!
  \*********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("(function(){\r\n  var crypt = __webpack_require__(/*! crypt */ \"./node_modules/crypt/crypt.js\"),\r\n      utf8 = __webpack_require__(/*! charenc */ \"./node_modules/charenc/charenc.js\").utf8,\r\n      isBuffer = __webpack_require__(/*! is-buffer */ \"./node_modules/is-buffer/index.js\"),\r\n      bin = __webpack_require__(/*! charenc */ \"./node_modules/charenc/charenc.js\").bin,\r\n\r\n  // The core\r\n  md5 = function (message, options) {\r\n    // Convert to byte array\r\n    if (message.constructor == String)\r\n      if (options && options.encoding === 'binary')\r\n        message = bin.stringToBytes(message);\r\n      else\r\n        message = utf8.stringToBytes(message);\r\n    else if (isBuffer(message))\r\n      message = Array.prototype.slice.call(message, 0);\r\n    else if (!Array.isArray(message) && message.constructor !== Uint8Array)\r\n      message = message.toString();\r\n    // else, assume byte array already\r\n\r\n    var m = crypt.bytesToWords(message),\r\n        l = message.length * 8,\r\n        a =  1732584193,\r\n        b = -271733879,\r\n        c = -1732584194,\r\n        d =  271733878;\r\n\r\n    // Swap endian\r\n    for (var i = 0; i < m.length; i++) {\r\n      m[i] = ((m[i] <<  8) | (m[i] >>> 24)) & 0x00FF00FF |\r\n             ((m[i] << 24) | (m[i] >>>  8)) & 0xFF00FF00;\r\n    }\r\n\r\n    // Padding\r\n    m[l >>> 5] |= 0x80 << (l % 32);\r\n    m[(((l + 64) >>> 9) << 4) + 14] = l;\r\n\r\n    // Method shortcuts\r\n    var FF = md5._ff,\r\n        GG = md5._gg,\r\n        HH = md5._hh,\r\n        II = md5._ii;\r\n\r\n    for (var i = 0; i < m.length; i += 16) {\r\n\r\n      var aa = a,\r\n          bb = b,\r\n          cc = c,\r\n          dd = d;\r\n\r\n      a = FF(a, b, c, d, m[i+ 0],  7, -680876936);\r\n      d = FF(d, a, b, c, m[i+ 1], 12, -389564586);\r\n      c = FF(c, d, a, b, m[i+ 2], 17,  606105819);\r\n      b = FF(b, c, d, a, m[i+ 3], 22, -1044525330);\r\n      a = FF(a, b, c, d, m[i+ 4],  7, -176418897);\r\n      d = FF(d, a, b, c, m[i+ 5], 12,  1200080426);\r\n      c = FF(c, d, a, b, m[i+ 6], 17, -1473231341);\r\n      b = FF(b, c, d, a, m[i+ 7], 22, -45705983);\r\n      a = FF(a, b, c, d, m[i+ 8],  7,  1770035416);\r\n      d = FF(d, a, b, c, m[i+ 9], 12, -1958414417);\r\n      c = FF(c, d, a, b, m[i+10], 17, -42063);\r\n      b = FF(b, c, d, a, m[i+11], 22, -1990404162);\r\n      a = FF(a, b, c, d, m[i+12],  7,  1804603682);\r\n      d = FF(d, a, b, c, m[i+13], 12, -40341101);\r\n      c = FF(c, d, a, b, m[i+14], 17, -1502002290);\r\n      b = FF(b, c, d, a, m[i+15], 22,  1236535329);\r\n\r\n      a = GG(a, b, c, d, m[i+ 1],  5, -165796510);\r\n      d = GG(d, a, b, c, m[i+ 6],  9, -1069501632);\r\n      c = GG(c, d, a, b, m[i+11], 14,  643717713);\r\n      b = GG(b, c, d, a, m[i+ 0], 20, -373897302);\r\n      a = GG(a, b, c, d, m[i+ 5],  5, -701558691);\r\n      d = GG(d, a, b, c, m[i+10],  9,  38016083);\r\n      c = GG(c, d, a, b, m[i+15], 14, -660478335);\r\n      b = GG(b, c, d, a, m[i+ 4], 20, -405537848);\r\n      a = GG(a, b, c, d, m[i+ 9],  5,  568446438);\r\n      d = GG(d, a, b, c, m[i+14],  9, -1019803690);\r\n      c = GG(c, d, a, b, m[i+ 3], 14, -187363961);\r\n      b = GG(b, c, d, a, m[i+ 8], 20,  1163531501);\r\n      a = GG(a, b, c, d, m[i+13],  5, -1444681467);\r\n      d = GG(d, a, b, c, m[i+ 2],  9, -51403784);\r\n      c = GG(c, d, a, b, m[i+ 7], 14,  1735328473);\r\n      b = GG(b, c, d, a, m[i+12], 20, -1926607734);\r\n\r\n      a = HH(a, b, c, d, m[i+ 5],  4, -378558);\r\n      d = HH(d, a, b, c, m[i+ 8], 11, -2022574463);\r\n      c = HH(c, d, a, b, m[i+11], 16,  1839030562);\r\n      b = HH(b, c, d, a, m[i+14], 23, -35309556);\r\n      a = HH(a, b, c, d, m[i+ 1],  4, -1530992060);\r\n      d = HH(d, a, b, c, m[i+ 4], 11,  1272893353);\r\n      c = HH(c, d, a, b, m[i+ 7], 16, -155497632);\r\n      b = HH(b, c, d, a, m[i+10], 23, -1094730640);\r\n      a = HH(a, b, c, d, m[i+13],  4,  681279174);\r\n      d = HH(d, a, b, c, m[i+ 0], 11, -358537222);\r\n      c = HH(c, d, a, b, m[i+ 3], 16, -722521979);\r\n      b = HH(b, c, d, a, m[i+ 6], 23,  76029189);\r\n      a = HH(a, b, c, d, m[i+ 9],  4, -640364487);\r\n      d = HH(d, a, b, c, m[i+12], 11, -421815835);\r\n      c = HH(c, d, a, b, m[i+15], 16,  530742520);\r\n      b = HH(b, c, d, a, m[i+ 2], 23, -995338651);\r\n\r\n      a = II(a, b, c, d, m[i+ 0],  6, -198630844);\r\n      d = II(d, a, b, c, m[i+ 7], 10,  1126891415);\r\n      c = II(c, d, a, b, m[i+14], 15, -1416354905);\r\n      b = II(b, c, d, a, m[i+ 5], 21, -57434055);\r\n      a = II(a, b, c, d, m[i+12],  6,  1700485571);\r\n      d = II(d, a, b, c, m[i+ 3], 10, -1894986606);\r\n      c = II(c, d, a, b, m[i+10], 15, -1051523);\r\n      b = II(b, c, d, a, m[i+ 1], 21, -2054922799);\r\n      a = II(a, b, c, d, m[i+ 8],  6,  1873313359);\r\n      d = II(d, a, b, c, m[i+15], 10, -30611744);\r\n      c = II(c, d, a, b, m[i+ 6], 15, -1560198380);\r\n      b = II(b, c, d, a, m[i+13], 21,  1309151649);\r\n      a = II(a, b, c, d, m[i+ 4],  6, -145523070);\r\n      d = II(d, a, b, c, m[i+11], 10, -1120210379);\r\n      c = II(c, d, a, b, m[i+ 2], 15,  718787259);\r\n      b = II(b, c, d, a, m[i+ 9], 21, -343485551);\r\n\r\n      a = (a + aa) >>> 0;\r\n      b = (b + bb) >>> 0;\r\n      c = (c + cc) >>> 0;\r\n      d = (d + dd) >>> 0;\r\n    }\r\n\r\n    return crypt.endian([a, b, c, d]);\r\n  };\r\n\r\n  // Auxiliary functions\r\n  md5._ff  = function (a, b, c, d, x, s, t) {\r\n    var n = a + (b & c | ~b & d) + (x >>> 0) + t;\r\n    return ((n << s) | (n >>> (32 - s))) + b;\r\n  };\r\n  md5._gg  = function (a, b, c, d, x, s, t) {\r\n    var n = a + (b & d | c & ~d) + (x >>> 0) + t;\r\n    return ((n << s) | (n >>> (32 - s))) + b;\r\n  };\r\n  md5._hh  = function (a, b, c, d, x, s, t) {\r\n    var n = a + (b ^ c ^ d) + (x >>> 0) + t;\r\n    return ((n << s) | (n >>> (32 - s))) + b;\r\n  };\r\n  md5._ii  = function (a, b, c, d, x, s, t) {\r\n    var n = a + (c ^ (b | ~d)) + (x >>> 0) + t;\r\n    return ((n << s) | (n >>> (32 - s))) + b;\r\n  };\r\n\r\n  // Package private blocksize\r\n  md5._blocksize = 16;\r\n  md5._digestsize = 16;\r\n\r\n  module.exports = function (message, options) {\r\n    if (message === undefined || message === null)\r\n      throw new Error('Illegal argument ' + message);\r\n\r\n    var digestbytes = crypt.wordsToBytes(md5(message, options));\r\n    return options && options.asBytes ? digestbytes :\r\n        options && options.asString ? bin.bytesToString(digestbytes) :\r\n        crypt.bytesToHex(digestbytes);\r\n  };\r\n\r\n})();\r\n\n\n//# sourceURL=webpack://word-code/./node_modules/md5/md5.js?");

/***/ }),

/***/ "./src/base/base.mjs":
/*!***************************!*\
  !*** ./src/base/base.mjs ***!
  \***************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Base)\n/* harmony export */ });\n/* harmony import */ var _described_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./described.mjs */ \"./src/base/described.mjs\");\n/* harmony import */ var _browser_mjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./browser.mjs */ \"./src/base/browser.mjs\");\n/* harmony import */ var md5__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! md5 */ \"./node_modules/md5/md5.js\");\n/* harmony import */ var _logic_logic_mjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../logic/logic.mjs */ \"./src/logic/logic.mjs\");\n\n\n\n\n\nclass Base {\n\n  constructor(params = {}) {\n    this.described = _described_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.browser = new _browser_mjs__WEBPACK_IMPORTED_MODULE_1__[\"default\"]();\n    this.logic = new _logic_logic_mjs__WEBPACK_IMPORTED_MODULE_3__[\"default\"]();\n    this.md5 = md5__WEBPACK_IMPORTED_MODULE_2__;\n  }\n\n  // 判断值原始类型\n  // 基础typeof在类型表达上过于模糊例如3和3.14同属于number, 利用正则解析字符串进行相对准确判断\n  _typeof(value) {\n    if (!!!value) return typeof value;\n    // if (typeof value == 'string') return 'string' // 字符串类型\n\n    let valueStr = value.toString()\n    if (typeof value == 'object') {\n      if (Array.isArray(value)) { // 数组对象类型\n        return 'array'\n      } else if (!!(value.__proto__.toString().match(/^\\[object .*Element\\]$/))) { // html元素类型\n        return 'element'\n      } else {\n        return 'keyValue' // 键值对类型\n      }\n    } else if (valueStr.match(/^http(s|)\\:\\/\\//)) { // 链接类型\n      return 'url'\n    } else if (!!valueStr.match(/^(\\-|)\\d{1,}$/)) { // 数值类型(整数)\n      return 'integer'\n    } else if (!!valueStr.match(/^(\\-|)\\d{1,}\\.{1}\\d{0,2}$/)) { // 数值类型(2位小数)\n      return 'decimal(2)'\n    } else if (!!valueStr.match(/^(\\-|)\\d{1,}\\.{1}\\d{2,}$/)) { // 数值类型(大于2位小数)\n      return 'decimal(2+)'\n    } else if (!!valueStr.match(/^\\d{4}\\-\\d{2}\\-\\d{2}$/)) { // 日期类型\n      return 'date'\n    } else if (!!valueStr.match(/^\\d{4}\\-\\d{2}\\-\\d{2}T/)) { // 日期时间类型\n      return 'dateTime'\n    } else { // 其他按原始类型\n      return typeof value\n    }\n  }\n\n  // 多类型可选数据格式验证\n  multiType(mtype) {\n    let mapping = {\n      // 名称规则 基础类型在前,其他类型在后\n      'stringORfunction': (type) => { return ['string', 'function'].includes(type) },\n      'stringORinteger': (type) => { return ['string', 'integer'].includes(type) },\n    }\n    let defaultLambda = () => { return false }\n    return mapping[mtype] || defaultLambda\n  }\n\n  uuid() {\n    var s = [];\n    var hexDigits = \"0123456789abcdef\";\n    for (var i = 0; i < 36; i++) {\n      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);\n    }\n    s[14] = \"4\";  // bits 12-15 of the time_hi_and_version field to 0010\n    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01\n    s[8] = s[13] = s[18] = s[23] = \"-\";\n    var uuid = s.join(\"\");\n    return uuid;\n  }\n\n  hashCode(string) {\n    return string.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)\n  }\n\n}\n\n//# sourceURL=webpack://word-code/./src/base/base.mjs?");

/***/ }),

/***/ "./src/base/browser.mjs":
/*!******************************!*\
  !*** ./src/base/browser.mjs ***!
  \******************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Browser)\n/* harmony export */ });\nclass Browser {\n  // 根据制造商判断浏览器名称\n  browserName(){\n    let browserName = ''\n    let vender = window.navigator.vendor\n    switch (vender) {\n      case 'Google Inc.':\n        browserName = 'chrome'\n        break;\n      case 'Apple Computer, Inc.':\n        browserName = 'safair'\n        break;\n      default:\n        console.error('尚未定义制造商:'+vender+'对应浏览器名称')\n        browserName = '*'\n        break;\n    }\n    return browserName\n  }\n}\n\n//# sourceURL=webpack://word-code/./src/base/browser.mjs?");

/***/ }),

/***/ "./src/base/described.mjs":
/*!********************************!*\
  !*** ./src/base/described.mjs ***!
  \********************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Described)\n/* harmony export */ });\n/* harmony import */ var _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base/base.mjs */ \"./src/base/base.mjs\");\n// Base和Described相互引入 不能继承\n\nclass Described {\n  static initialize(params = {}) {\n    return new Described(params)\n  }\n\n  constructor(params = {}) {\n    // 对于需要用到的Base方法进行实现\n    let methodNames = ['_typeof', 'multiType', 'uuid']\n    methodNames.forEach(method => {\n      this[method] = (params) => { return (new _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"]())[method](params) }\n    })\n\n    let defaultValue = {\n      名称: undefined,\n      描述: undefined,\n      参数: {},\n    }\n\n    params = Object.assign(defaultValue, params)\n    params.参数 = Object.assign({\n\n    }, params.参数)\n\n    for (let [key, value] of Object.entries(params)) {\n      this[key] = value\n    }\n\n  }\n\n  columns() {\n    return Object.keys(this.参数)\n  }\n\n  validate(params = {}) {\n    let res = this.validate_empty(params)\n    if (!res[0]) return res\n\n    res = this.validate_type(params)\n    if (!res[0]) return res\n\n    return [true, '']\n  }\n\n  validate_empty(params = {}) {\n    if (!!!this.名称) return [false, \"描述信息中[名称]参数不能为空\"]\n    if (!!!this.描述) return [false, \"描述信息中[描述]参数不能为空\"]\n\n    for (let [key, value] of Object.entries(this.参数)) {\n      if ((params[key] == undefined) && (value.默认值 != undefined)) {\n        this[key] = value.默认值\n      } else if ((params[key] == undefined) && value.默认值 == undefined) {\n        let template = key + '为必填项, 不能为空'\n        if (!!value.描述) template += ', 用于: ' + value.描述\n        template += '.'\n        return [false, template]\n      } else {\n        this[key] = params[key]\n      }\n    }\n    return [true, '']\n  }\n\n  validate_type(params = {}) {\n    for (let [key, value] of Object.entries(this.参数)) {\n      let current_type = this._typeof(this[key])\n      if (!(current_type == value.类型)) {\n        if (this.multiType(value.类型)(current_type)) continue\n        let template = key + ' 类型不符,要求类型为: ' + value.类型 + ', 传递参数类型为: ' + current_type + '.'\n        return [false, template]\n      }\n    }\n    return [true, '']\n  }\n\n  createEle(params) {\n    let elementInfo = {}\n    Object.keys(params).filter(key => { return !this.columns().includes(key) }).forEach(item => {\n      elementInfo[item] = params[item]\n    })\n\n    let res = this.toElement(Object.assign({\n      element: 'div',\n      id: this.元素ID\n    }, elementInfo))\n\n    if (!res[0]) return res\n    this.element = res[1]\n    this.insertEle(this.element)\n\n    return [true, '']\n  }\n\n  insertEle(ele) {\n    let childEle = document.getElementById(ele.id || this.uuid())\n    if (!!childEle) {\n      childEle.remove()\n    }\n\n    if (this.插入模式 == '后置') {\n      this.父元素.append(ele)\n    } else if (this.插入模式 == '前置') {\n      this.父元素.prepend(ele)\n    } else {\n      this.插入模式(ele)\n    }\n  }\n\n}\n\n//# sourceURL=webpack://word-code/./src/base/described.mjs?");

/***/ }),

/***/ "./src/init.mjs":
/*!**********************!*\
  !*** ./src/init.mjs ***!
  \**********************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _word_code_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./word-code.mjs */ \"./src/word-code.mjs\");\n/* harmony import */ var _test_test_mjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./test/test.mjs */ \"./src/test/test.mjs\");\n/* harmony import */ var _scene_scene_mjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scene/scene.mjs */ \"./src/scene/scene.mjs\");\n\nwindow.WordCode = new _word_code_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\n\n\nwindow.WordCode.test = new _test_test_mjs__WEBPACK_IMPORTED_MODULE_1__[\"default\"]();\n\n\nwindow.WordCode.scene = new _scene_scene_mjs__WEBPACK_IMPORTED_MODULE_2__[\"default\"]();\n\n\n//# sourceURL=webpack://word-code/./src/init.mjs?");

/***/ }),

/***/ "./src/logic/logic.mjs":
/*!*****************************!*\
  !*** ./src/logic/logic.mjs ***!
  \*****************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Logic)\n/* harmony export */ });\nclass Logic {\n\n  constructor(params = {}) {\n    // super();\n    let defaultValue = {\n    }\n    params = Object.assign(defaultValue, params)\n    for (let [key, value] of Object.entries(defaultValue)) {\n      this[key] = value\n    }\n  }\n\n  judge(actual, type, expect) {\n    let flag\n    // 对于进行大小比较的值进行格式化处理\n    let handelCompareValue = (value) => {\n      let result = value\n\n      switch (typeof (value)) {\n        case 'string':\n          if (/^\\d{1,}\\.\\d{1,}$/.test(value)) {\n            result = parseFloat(value)\n          } else if (/^\\d{1,}$/.test(value)) {\n            result = parseInt(value)\n          }\n          break;\n      }\n      return result\n    }\n\n    if (/.*于.*/.test(type)) {\n      actual = handelCompareValue(actual)\n      expect = handelCompareValue(expect)\n    }\n    switch (true) {\n      case /小于$/.test(type):\n        flag = (actual < expect)\n        break;\n      case /小于等于/.test(type):\n        flag = (actual <= expect)\n        break;\n      case /大于等于/.test(type):\n        flag = (actual >= expect)\n        break;\n      case /等于/.test(type):\n        flag = (actual == expect)\n        break;\n      case /大于$/.test(type):\n        flag = (actual > expect)\n        break;\n      case /匹配/.test(type):\n        flag = (!!actual.toString().match(new RegExp(expect)))\n        break;\n      default:\n        flag = null\n        break;\n    }\n\n    if (flag == null) return flag;\n    if (/^不.*/.test(type)) return !flag;\n\n    return flag\n  }\n}\n\n//# sourceURL=webpack://word-code/./src/logic/logic.mjs?");

/***/ }),

/***/ "./src/scene/scene.mjs":
/*!*****************************!*\
  !*** ./src/scene/scene.mjs ***!
  \*****************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Scene)\n/* harmony export */ });\n/* harmony import */ var _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base/base.mjs */ \"./src/base/base.mjs\");\n\nclass Scene extends _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"] {\n\n  constructor(params = {}) {\n    super();\n    let defaultValue = {\n    }\n    params = Object.assign(defaultValue, params)\n    for (let [key, value] of Object.entries(defaultValue)) {\n      this[key] = value\n    }\n  }\n\n  deduce(params = {}) {\n    let described = this.described.initialize({\n      名称: '场景推演',\n      描述: '根据提供的验证对象及相关验证场景, 推演出给对对象所符合场景.',\n      参数: {\n        验证对象: { 类型: 'keyValue', 描述: '' },\n        验证场景: { 类型: 'keyValue' },\n      }\n    })\n    if (!!params.showHow) return [true, described]\n\n    let res = described.validate(params)\n    if (!res[0]) return res\n    let result = { 校验详情: {} }\n\n    for (let [scene_name, scene] of Object.entries(described.验证场景)) {\n\n      for (let [key, condition] of Object.entries(scene)) {\n        let actual = described.验证对象[key]\n        let judge_type = Object.keys(condition)[0]\n        let expect = Object.values(condition)[0]\n        let flag = this.logic.judge(actual, judge_type, expect)\n        let message = '满足: [' + key + '] 条件, 期望: ' + key + judge_type + expect + ', 实际值: ' + actual + ' .'\n        result['校验详情'][scene_name] ||= { 满足: [], 不满足: [] }\n\n        if (flag) {\n          result['校验详情'][scene_name]['满足'].push(message)\n          if ((Object.keys(scene).pop() == key) && result['校验详情'][scene_name]['不满足'].length == 0) {\n            result.满足场景名称 = scene_name\n            return [true, '符合' + scene_name + '场景', result]\n          }\n        } else {\n          result['校验详情'][scene_name]['不满足'].push('不' + message)\n          break\n        }\n      }\n    }\n    return [false, '所有场景均不符合', result]\n\n    /**\n     \n      WordCode.scene.deduce({\n        验证对象: {\"基金简称\":\"上投摩根新兴动力混合A\",\"基金代码\":\"377240\",\"净值日期\":\"2021-04-30\",\"单位净值\":\"6.1720\",\"累计净值\":\"6.1720\",\"日增长率\":\"0.95\"},\n        验证场景: {\n          '涨超5%': {日增长率: {大于: 5}},\n          '涨超4%': {日增长率: {大于: 4}},\n          '涨超3%': {日增长率: {大于: 3}},\n          '涨超2%': {日增长率: {大于: 2}},\n          '轻微上扬': {日增长率: {大于: 0}},\n          当日下跌: {日增长率: {小于: 0}},\n        }\n      })\n\n    */\n  }\n}\n\n\n//# sourceURL=webpack://word-code/./src/scene/scene.mjs?");

/***/ }),

/***/ "./src/test/logic/logic.mjs":
/*!**********************************!*\
  !*** ./src/test/logic/logic.mjs ***!
  \**********************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n  逻辑判断: {\n    基础逻辑判断: {\n      函数: 'WordCode.logic.judge',\n      回测用例: {\n        \"数据类型相同-小于号-字符串类型-1\": { 参数: ['6.1720', '小于', '6.0'], 期望值: false },\n        \"数据类型相同-小于号-字符串类型-2\": { 参数: ['6.1720', '小于', '7.0'], 期望值: true },\n        \"数据类型相同-小于号-字符串类型-3\": { 参数: ['6.1720', '小于', '6.1720'], 期望值: false },\n        \"数据类型相同-小于号-数值类型-1\": { 参数: [6.1720, '小于', 6.0], 期望值: false },\n        \"数据类型相同-小于号-数值类型-2\": { 参数: [6.1720, '小于', 7.0], 期望值: true },\n        \"数据类型相同-小于号-数值类型-3\": { 参数: [6.1720, '小于', 6.1720], 期望值: false },\n        \"数据类型相同-小于号-数值类型-4\": { 参数: [6.1720, '小于', 6.172000], 期望值: false },\n        \"数据类型相同-小于号-数值类型-5\": { 参数: [6.1720, '小于', 6.172], 期望值: false },\n        \"数据类型相同-小于号-日期类型-1\": { 参数: ['2021-04-30', '小于', '2021-04-31'], 期望值: true },\n        \"数据类型相同-小于号-日期类型-2\": { 参数: ['2021-04-30', '小于', '2021-04-29'], 期望值: false },\n        \"数据类型相同-小于号-日期类型-3\": { 参数: ['2021-04-30', '小于', '2021-04-30'], 期望值: false },\n        \"数据类型相同-小于等于号-字符串类型-1\": { 参数: ['6.1720', '小于等于', '6.1720'], 期望值: true },\n        \"数据类型相同-小于等于号-字符串类型-2\": { 参数: ['6.1720', '小于等于', '6.172'], 期望值: true },\n        \"数据类型相同-小于等于号-字符串类型-3\": { 参数: ['6.1720', '小于等于', '6.17'], 期望值: false },\n        \"数据类型相同-小于等于号-字符串类型-4\": { 参数: ['6.1720', '小于等于', '6.17000'], 期望值: false },\n        \"数据类型相同-小于等于号-数值类型-1\": { 参数: [5.1720, '小于等于', 6.0], 期望值: true },\n        \"数据类型相同-小于等于号-数值类型-2\": { 参数: [5.1720, '小于等于', 5.1720], 期望值: true },\n        \"数据类型相同-小于等于号-数值类型-3\": { 参数: [5.1720, '小于等于', 5.1720000], 期望值: true },\n        \"数据类型相同-小于等于号-数值类型-4\": { 参数: [5.1720, '小于等于', 5.172], 期望值: true },\n        \"数据类型相同-小于等于号-数值类型-1\": { 参数: [5.1720, '小于等于', 5.2], 期望值: true },\n        \"数据类型相同-小于等于号-日期类型-1\": { 参数: ['2021-04-30', '小于等于', '2021-04-31'], 期望值: true },\n        \"数据类型相同-小于等于号-日期类型-2\": { 参数: ['2021-04-30', '小于等于', '2021-04-30'], 期望值: true },\n        \"数据类型相同-小于等于号-日期类型-1\": { 参数: ['2021-04-30', '小于等于', '2021-04-29'], 期望值: false },\n        \"数据类型相同-等于号-字符串类型-1\": { 参数: ['6.1720', '等于', '6.0'], 期望值: false },\n        \"数据类型相同-等于号-字符串类型-2\": { 参数: ['6.1720', '等于', '6.172'], 期望值: true },\n        \"数据类型相同-等于号-字符串类型-3\": { 参数: ['6.1720', '等于', '6.17200'], 期望值: true },\n        \"数据类型相同-等于号-字符串类型-4\": { 参数: ['6.1720', '等于', '5.0'], 期望值: false },\n        \"数据类型相同-等于号-数值类型-1\": { 参数: [6.1720, '等于', 6.0], 期望值: false },\n        \"数据类型相同-等于号-数值类型-2\": { 参数: [6.1720, '等于', 6.1720], 期望值: true },\n        \"数据类型相同-等于号-数值类型-3\": { 参数: [6.1720, '等于', 6.172], 期望值: true },\n        \"数据类型相同-等于号-日期类型-1\": { 参数: ['2021-04-30', '等于', '2021-04-31'], 期望值: false },\n        \"数据类型相同-等于号-日期类型-2\": { 参数: ['2021-04-30', '等于', '2021-04-30'], 期望值: true },\n        \"数据类型相同-等于号-日期类型-3\": { 参数: ['2021-04-30', '等于', '2021-04-29'], 期望值: false },\n        \"数据类型相同-大于等于号-字符串类型-1\": { 参数: ['6.1720', '大于等于', '6.0'], 期望值: true },\n        \"数据类型相同-大于等于号-字符串类型-2\": { 参数: ['6.1720', '大于等于', '6.172'], 期望值: true },\n        \"数据类型相同-大于等于号-字符串类型-3\": { 参数: ['6.1720', '大于等于', '6.17'], 期望值: true },\n        \"数据类型相同-大于等于号-数值类型-1\": { 参数: [6.1720, '大于等于', 6.17200], 期望值: true },\n        \"数据类型相同-大于等于号-数值类型-2\": { 参数: [6.1720, '大于等于', 6.172], 期望值: true },\n        \"数据类型相同-大于等于号-数值类型-3\": { 参数: [6.1720, '大于等于', 6.17], 期望值: true },\n        \"数据类型相同-大于等于号-日期类型-1\": { 参数: ['2021-04-30', '大于等于', '2021-04-31'], 期望值: false },\n        \"数据类型相同-大于等于号-日期类型-2\": { 参数: ['2021-04-30', '大于等于', '2021-04-30'], 期望值: true },\n        \"数据类型相同-大于等于号-日期类型-3\": { 参数: ['2021-04-30', '大于等于', '2021-04-29'], 期望值: true },\n        \"数据类型相同-大于号-字符串类型-1\": { 参数: ['6.1720', '大于', '6.0'], 期望值: true },\n        \"数据类型相同-大于号-字符串类型-2\": { 参数: ['6.1720', '大于', '6.172'], 期望值: false },\n        \"数据类型相同-大于号-字符串类型-3\": { 参数: ['6.1720', '大于', '6.17'], 期望值: true },\n        \"数据类型相同-大于号-数值类型-1\": { 参数: [6.1720, '大于', 6.0], 期望值: true },\n        \"数据类型相同-大于号-数值类型-2\": { 参数: [6.1720, '大于', 6.172], 期望值: false },\n        \"数据类型相同-大于号-数值类型-3\": { 参数: [6.1720, '大于', 6.17], 期望值: true },\n        \"数据类型相同-大于号-数值类型-4\": { 参数: [6.1720, '大于', 5.17], 期望值: true },\n        \"数据类型相同-大于号-日期类型-1\": { 参数: ['2021-04-30', '大于', '2021-04-31'], 期望值: false },\n        \"数据类型相同-大于号-日期类型-2\": { 参数: ['2021-04-30', '大于', '2021-04-29'], 期望值: true },\n        \"数据类型相同-大于号-日期类型-3\": { 参数: ['2020-04-30', '大于', '2021-04-29'], 期望值: false },\n        \"数据类型相同-匹配号-字符串类型\": { 参数: ['6.1720', '匹配', '6.0'], 期望值: false },\n        \"数据类型相同-匹配号-数值类型\": { 参数: [6.1720, '匹配', 6.0], 期望值: true },\n        \"数据类型相同-匹配号-日期类型\": { 参数: ['2021-04-30', '匹配', '\\\\d{4}-\\\\d{2}-\\\\d{2}'], 期望值: true },\n      }\n    }\n  }\n});\n\n//# sourceURL=webpack://word-code/./src/test/logic/logic.mjs?");

/***/ }),

/***/ "./src/test/scene/scene.mjs":
/*!**********************************!*\
  !*** ./src/test/scene/scene.mjs ***!
  \**********************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\nlet baseObject = {\n  验证对象: { \"基金简称\": \"上投摩根新兴动力混合A\", \"基金代码\": \"377240\", \"净值日期\": \"2021-04-30\", \"单位净值\": \"6.1720\", \"累计净值\": \"6.1720\", \"日增长率\": \"0.95\" },\n  验证场景: {\n    '涨超5%': { 日增长率: { 大于: 5 } },\n    '涨超4%': { 日增长率: { 大于: 4 } },\n    '涨超3%': { 日增长率: { 大于: 3 } },\n    '涨超2%': { 日增长率: { 大于: 2 } },\n    '轻微上扬': { 日增长率: { 大于: 0 } },\n    当日下跌: { 日增长率: { 小于: 0 } },\n  }\n}\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n  场景: {\n    场景验证: {\n      函数: 'WordCode.scene.deduce',\n      回测用例: {\n        基础测试: {参数: [baseObject], 期望值: function(result){return (result[0] == true && result[2].满足场景名称 == '轻微上扬')}},\n      }\n    }\n  }\n});\n\n//# sourceURL=webpack://word-code/./src/test/scene/scene.mjs?");

/***/ }),

/***/ "./src/test/test.mjs":
/*!***************************!*\
  !*** ./src/test/test.mjs ***!
  \***************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Test)\n/* harmony export */ });\n/* harmony import */ var _logic_logic_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./logic/logic.mjs */ \"./src/test/logic/logic.mjs\");\n/* harmony import */ var _scene_scene_mjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scene/scene.mjs */ \"./src/test/scene/scene.mjs\");\n\n\n\nclass Test {\n  constructor(params = {}) {\n    let defaultValue = {\n      // Object.assign({}, ...[{test: 'value-1'}, {name: 'value-2'}])\n      测试用例: (Object.assign({}, ...[_logic_logic_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"], _scene_scene_mjs__WEBPACK_IMPORTED_MODULE_1__[\"default\"]]))\n    }\n    params = Object.assign(defaultValue, params)\n    for (let [key, value] of Object.entries(defaultValue)) {\n      this[key] = value\n    }\n  }\n\n  all() {\n    let result = { all: [], errors: [] }\n    for (let [class_name, functions] of Object.entries(this.测试用例)) {\n      for (let [func_name, info] of Object.entries(functions)) {\n        let func = eval(info.函数)\n        for (let [case_name, case_] of Object.entries(info.回测用例)) {\n          let flag\n          if (typeof case_.期望值 == 'function') {\n            let res = func.bind(WordCode).apply(null, case_.参数)\n            flag = (!!case_.期望值(res))\n          } else {\n            flag = (func.apply(null, case_.参数) == case_.期望值)\n          }\n\n          let tmp = {\n            '类名#方法名': class_name+'#'+func_name + '(' + info.函数 + ')',\n            测试场景: case_name,\n            用例参数: JSON.stringify(case_.参数),\n            期望值: case_.期望值,\n            是否符合预期: (flag ? '是' : '否'),\n          }\n          result.all.push(tmp)\n          if (!flag) result.errors.push(tmp);\n\n        }\n      }\n    }\n    return result\n  }\n\n  testResult() {\n    let result = this.all()\n    console.table(result.all)\n    if (result.errors.length > 0) console.table(result.errors);\n  }\n\n}\n\n\n\n//# sourceURL=webpack://word-code/./src/test/test.mjs?");

/***/ }),

/***/ "./src/word-code.mjs":
/*!***************************!*\
  !*** ./src/word-code.mjs ***!
  \***************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ WordCode)\n/* harmony export */ });\n/* harmony import */ var _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base/base.mjs */ \"./src/base/base.mjs\");\n\nclass WordCode extends _base_base_mjs__WEBPACK_IMPORTED_MODULE_0__[\"default\"] {\n\n  constructor(params = {}) {\n    super();\n    let defaultValue = {\n    }\n    params = Object.assign(defaultValue, params)\n    for (let [key, value] of Object.entries(defaultValue)) {\n      this[key] = value\n    }\n  }\n\n}\n\n//# sourceURL=webpack://word-code/./src/word-code.mjs?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/init.mjs");
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});