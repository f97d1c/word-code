import logic from './logic/logic.mjs'
import scene from './scene/scene.mjs'

export default class Test {
  constructor(params = {}) {
    let defaultValue = {
      // Object.assign({}, ...[{test: 'value-1'}, {name: 'value-2'}])
      测试用例: (Object.assign({}, ...[logic, scene]))
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }
  }

  all() {
    let result = { all: [], errors: [] }
    for (let [class_name, functions] of Object.entries(this.测试用例)) {
      for (let [func_name, info] of Object.entries(functions)) {
        let func = eval(info.函数)
        for (let [case_name, case_] of Object.entries(info.回测用例)) {
          let flag
          if (typeof case_.期望值 == 'function') {
            let res = func.bind(WordCode).apply(null, case_.参数)
            flag = (!!case_.期望值(res))
          } else {
            flag = (func.apply(null, case_.参数) == case_.期望值)
          }

          let tmp = {
            '类名#方法名': class_name+'#'+func_name + '(' + info.函数 + ')',
            测试场景: case_name,
            用例参数: JSON.stringify(case_.参数),
            期望值: case_.期望值,
            是否符合预期: (flag ? '是' : '否'),
          }
          result.all.push(tmp)
          if (!flag) result.errors.push(tmp);

        }
      }
    }
    return result
  }

  testResult() {
    let result = this.all()
    console.table(result.all)
    if (result.errors.length > 0) console.table(result.errors);
  }

}

