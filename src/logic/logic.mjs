export default class Logic {

  constructor(params = {}) {
    // super();
    let defaultValue = {
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }
  }

  judge(actual, type, expect) {
    let flag
    // 对于进行大小比较的值进行格式化处理
    let handelCompareValue = (value) => {
      let result = value

      switch (typeof (value)) {
        case 'string':
          if (/^\d{1,}\.\d{1,}$/.test(value)) {
            result = parseFloat(value)
          } else if (/^\d{1,}$/.test(value)) {
            result = parseInt(value)
          }
          break;
      }
      return result
    }

    if (/.*于.*/.test(type)) {
      actual = handelCompareValue(actual)
      expect = handelCompareValue(expect)
    }
    switch (true) {
      case /小于$/.test(type):
        flag = (actual < expect)
        break;
      case /小于等于/.test(type):
        flag = (actual <= expect)
        break;
      case /大于等于/.test(type):
        flag = (actual >= expect)
        break;
      case /等于/.test(type):
        flag = (actual == expect)
        break;
      case /大于$/.test(type):
        flag = (actual > expect)
        break;
      case /匹配/.test(type):
        flag = (!!actual.toString().match(new RegExp(expect)))
        break;
      default:
        flag = null
        break;
    }

    if (flag == null) return flag;
    if (/^不.*/.test(type)) return !flag;

    return flag
  }
}