import Base from '../base/base.mjs'
export default class Scene extends Base {

  constructor(params = {}) {
    super();
    let defaultValue = {
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }
  }

  deduce(params = {}) {
    let described = this.described.initialize({
      名称: '场景推演',
      描述: '根据提供的验证对象及相关验证场景, 推演出给对对象所符合场景.',
      参数: {
        验证对象: { 类型: 'keyValue', 描述: '' },
        验证场景: { 类型: 'keyValue' },
      }
    })
    if (!!params.showHow) return [true, described]

    let res = described.validate(params)
    if (!res[0]) return res
    let result = { 校验详情: {} }

    for (let [scene_name, scene] of Object.entries(described.验证场景)) {

      for (let [key, condition] of Object.entries(scene)) {
        let actual = described.验证对象[key]
        let judge_type = Object.keys(condition)[0]
        let expect = Object.values(condition)[0]
        let flag = this.logic.judge(actual, judge_type, expect)
        let message = '满足: [' + key + '] 条件, 期望: ' + key + judge_type + expect + ', 实际值: ' + actual + ' .'
        result['校验详情'][scene_name] ||= { 满足: [], 不满足: [] }

        if (flag) {
          result['校验详情'][scene_name]['满足'].push(message)
          if ((Object.keys(scene).pop() == key) && result['校验详情'][scene_name]['不满足'].length == 0) {
            result.满足场景名称 = scene_name
            return [true, '符合' + scene_name + '场景', result]
          }
        } else {
          result['校验详情'][scene_name]['不满足'].push('不' + message)
          break
        }
      }
    }
    return [false, '所有场景均不符合', result]

    /**
     
      WordCode.scene.deduce({
        验证对象: {"基金简称":"上投摩根新兴动力混合A","基金代码":"377240","净值日期":"2021-04-30","单位净值":"6.1720","累计净值":"6.1720","日增长率":"0.95"},
        验证场景: {
          '涨超5%': {日增长率: {大于: 5}},
          '涨超4%': {日增长率: {大于: 4}},
          '涨超3%': {日增长率: {大于: 3}},
          '涨超2%': {日增长率: {大于: 2}},
          '轻微上扬': {日增长率: {大于: 0}},
          当日下跌: {日增长率: {小于: 0}},
        }
      })

    */
  }
}
