import Described from './described.mjs';
import Browser from './browser.mjs';
import Md5 from 'md5';
import Logic from '../logic/logic.mjs';

export default class Base {

  constructor(params = {}) {
    this.described = Described;
    this.browser = new Browser();
    this.logic = new Logic();
    this.md5 = Md5;
  }

  // 判断值原始类型
  // 基础typeof在类型表达上过于模糊例如3和3.14同属于number, 利用正则解析字符串进行相对准确判断
  _typeof(value) {
    if (!!!value) return typeof value;
    // if (typeof value == 'string') return 'string' // 字符串类型

    let valueStr = value.toString()
    if (typeof value == 'object') {
      if (Array.isArray(value)) { // 数组对象类型
        return 'array'
      } else if (!!(value.__proto__.toString().match(/^\[object .*Element\]$/))) { // html元素类型
        return 'element'
      } else {
        return 'keyValue' // 键值对类型
      }
    } else if (valueStr.match(/^http(s|)\:\/\//)) { // 链接类型
      return 'url'
    } else if (!!valueStr.match(/^(\-|)\d{1,}$/)) { // 数值类型(整数)
      return 'integer'
    } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{0,2}$/)) { // 数值类型(2位小数)
      return 'decimal(2)'
    } else if (!!valueStr.match(/^(\-|)\d{1,}\.{1}\d{2,}$/)) { // 数值类型(大于2位小数)
      return 'decimal(2+)'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}$/)) { // 日期类型
      return 'date'
    } else if (!!valueStr.match(/^\d{4}\-\d{2}\-\d{2}T/)) { // 日期时间类型
      return 'dateTime'
    } else { // 其他按原始类型
      return typeof value
    }
  }

  // 多类型可选数据格式验证
  multiType(mtype) {
    let mapping = {
      // 名称规则 基础类型在前,其他类型在后
      'stringORfunction': (type) => { return ['string', 'function'].includes(type) },
      'stringORinteger': (type) => { return ['string', 'integer'].includes(type) },
    }
    let defaultLambda = () => { return false }
    return mapping[mtype] || defaultLambda
  }

  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
    var uuid = s.join("");
    return uuid;
  }

  hashCode(string) {
    return string.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)
  }

}