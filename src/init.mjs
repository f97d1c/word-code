import WordCode from './word-code.mjs';
window.WordCode = new WordCode();

import Test from './test/test.mjs';
window.WordCode.test = new Test();

import Scene from './scene/scene.mjs';
window.WordCode.scene = new Scene();
