import Base from './base/base.mjs'
export default class WordCode extends Base {

  constructor(params = {}) {
    super();
    let defaultValue = {
    }
    params = Object.assign(defaultValue, params)
    for (let [key, value] of Object.entries(defaultValue)) {
      this[key] = value
    }
  }

}